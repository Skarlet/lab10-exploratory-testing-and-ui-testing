import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestYouTube {

    @Test
    public void findVideo() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://www.youtube.com/");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("YouTube", driver.getTitle());
        WebElement findLine = driver.findElement(By.xpath("/html/body/ytd-app/div/div/ytd-masthead/div[3]/div[2]/ytd-searchbox/form/div[1]/div[1]/input"));
        findLine.sendKeys("Rick Astley - Never Gonna Give You Up (Video)");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/ytd-app/div/div/ytd-masthead/div[3]/div[2]/ytd-searchbox/form/button"));
        searchButton.click();
        searchButton = driver.findElement(By.xpath("/html/body/ytd-app/div/ytd-page-manager/ytd-search/div[1]/ytd-two-column-search-results-renderer/div/ytd-section-list-renderer/div[2]/ytd-item-section-renderer/div[3]/ytd-video-renderer[1]/div[1]/div"));
        searchButton.click();
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Rick Astley - Never Gonna Give You Up (Video) - YouTube", driver.getTitle());
        driver.quit();
    }
}
