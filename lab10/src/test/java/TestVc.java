import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestVc {
    @Test
    public void findArticle() {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://vc.ru/");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("vc.ru — бизнес, технологии, идеи, модели роста, стартапы", driver.getTitle());
        WebElement findLine = driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div[2]/input"));
        findLine.sendKeys("Tinkoff");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[2]/div[3]/div/div[2]/div/a"));
        searchButton.click();
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("vc.ru — бизнес, технологии, идеи, модели роста, стартапы", driver.getTitle());
        driver.quit();
    }
}
